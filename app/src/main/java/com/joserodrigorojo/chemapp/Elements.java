package com.joserodrigorojo.chemapp;

public class Elements {

    private String name;
    private String symbol;
    private String number;
    private String category;
    private String atomic_mass;
    private String summary;

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getCategory() {
        return category;
    }

    public String getNumber() {
        return number;
    }

    public String getMass() {
        return atomic_mass;
    }
    public String getSummary() {
        return summary;
    }
}