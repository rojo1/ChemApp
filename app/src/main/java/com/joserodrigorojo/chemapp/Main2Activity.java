package com.joserodrigorojo.chemapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.support.design.widget.Snackbar;
import java.util.ArrayList;
import java.util.HashMap;
import com.joserodrigorojo.chemapp.R;

import java.util.List;

public class Main2Activity extends AppCompatActivity implements LoadJSONTask.Listener, AdapterView.OnItemClickListener {

    private ListView mListView;

    public static final String URL = "https://cloud.joserodrigorojo.com/elements.json";

    private List<HashMap<String, String>> mAndroidMapList = new ArrayList<>();

    private static final String KEY_NAME = "name";
    private static final String KEY_SYMBOL = "symbol";
    private static final String KEY_NUMBER = "number";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_MASS = "mass";
    private static final String KEY_SUMMARY = "summary";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mListView = (ListView) findViewById(R.id.list_view);
        mListView.setOnItemClickListener(this);
        new LoadJSONTask(this).execute(URL);
    }

    @Override
    public void onLoaded(List<Elements> androidList) {

        for (Elements android : androidList) {

            HashMap<String, String> map = new HashMap<>();

            map.put(KEY_NAME, "Name: " + android.getName());
            map.put(KEY_SYMBOL, android.getSymbol());
            map.put(KEY_NUMBER, "Atomic Number: " + android.getNumber());
            map.put(KEY_CATEGORY,  android.getCategory());
            map.put(KEY_MASS, android.getMass());
            map.put(KEY_SUMMARY,  android.getSummary());

            mAndroidMapList.add(map);
        }

        loadListView();
    }

    @Override
    public void onError() {

        Snackbar.make(findViewById(R.id.myCoordinatorLayout), "There was a problem loading the list, make sure your are connected to the internet.",
                Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        TextView ChemName = (TextView) findViewById(R.id.text_view_id);
        ChemName.setText( mAndroidMapList.get(i).get(KEY_SUMMARY));
        TextView SymbolName = (TextView) findViewById(R.id.symbol);
        SymbolName.setText( mAndroidMapList.get(i).get(KEY_SYMBOL));
        TextView Name = (TextView) findViewById(R.id.text_name);
        Name.setText( mAndroidMapList.get(i).get(KEY_NAME));
        TextView Number = (TextView) findViewById(R.id.text_number);
        Number.setText( mAndroidMapList.get(i).get(KEY_NUMBER));
        TextView Phase = (TextView) findViewById(R.id.text_category);
        Phase.setText( mAndroidMapList.get(i).get(KEY_CATEGORY));
        TextView Mass = (TextView) findViewById(R.id.text_mass);
        Mass.setText( mAndroidMapList.get(i).get(KEY_MASS));

    }

    private void loadListView() {

        ListAdapter adapter = new SimpleAdapter(Main2Activity.this, mAndroidMapList, R.layout.list_item,
                new String[] {KEY_NUMBER, KEY_NAME},
                new int[] { R.id.version,R.id.name });

        mListView.setAdapter(adapter);

    }

}