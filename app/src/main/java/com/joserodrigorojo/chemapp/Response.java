package com.joserodrigorojo.chemapp;

/**
 * Created by poposes on 11/11/17.
 */
import java.util.ArrayList;
import java.util.List;

public class Response {

    private List<Elements> elements = new ArrayList<Elements>();

    public List<Elements> getElements() {
        return elements;
    }
}